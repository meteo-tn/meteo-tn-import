#!/bin/bash

# BACKUP
TODAY=`date +'%Y%m%d'`
pg_dump meteo | gzip > $TODAY_meteoTN.gz

# RESTORE
gunzip < $TODAY_meteoTN.gzip | psql meteo