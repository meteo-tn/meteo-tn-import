meteo-tn
========
Un progetto per migliorare l'accessibilità dei dati meteorologici trentini.

Perchè
------
Lunghe serie temporali sono critiche per l'analisi di moltissimi fenomeni con i quali dovremmo purtroppo sempre più confrontarci: il progetto nasce con l'intento di migliorare l'accessibilità a tali dati, sfruttando i servizi web messi a disposizione da meteotrentino.it.

Come
----
Il progetto nasce con l'idea di svilupparsi in svariati filoni, a seconda delle analisi che si desiderano affrontare e dei conseguenti dati di base necessari. Questi saranno al più frutto di postelaborazioni, avendo comunqe cura di partire da dati sempre disponibili online sotto licenze CC-BY o equivalenti, e rilasciati con attenzioni consimili.
Per ora, l'intento è quello di suddividere gli sforzi in almeno tre macroaree (in **grassetto** i progetti implementati):
	* dati di base localizzati (**stazioni meteo ufficiali**,stazioni meteo da *citizen science*)
	* dati satellitari (LANDSAT,Sentinel)
	* postelaborazioni (e.g., *kriging* dei dati meteo localizzati, variabili radiative aggregate e giornaliere da modelli digitali del terreno, indici vegetazionali e differenze, ...)
	* deflusso superficiale (in esplorazione)

Tecnologie
----------
	* Python (geopandas, keras)
	* PostgreSQL + PostGIS
	* GRASS GIS

Seguirà a breve una timeline degli sforzi, un'organizzazione dei progetti su questo archivio git e una guida per gli sviluppatori interessati a contribuire.